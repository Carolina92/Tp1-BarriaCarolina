#include <fstream>
#include <iostream>
#include <string>
#include <cstdio>
#include <stdlib.h>

using namespace std;

int getopcode(string asmline)               //recibe la linea de asembler(LOAD )
{
    int i=0;
    string operacion;                       //cadena de caracteres operacion
    i=asmline.find(" ");                    //encuentro posicion del espacio
    operacion=asmline.substr(0,i);          //substrae desde cero a la posicion i
    if(operacion=="LOAD")
    {
        return 0x01;
    }else if(operacion=="ADD")
    {
            return 0x02;
    }else if(operacion=="PRINT")
    {
        return 0x03;
    }else
    {
        return -1;                      //error
    }
}

int getparam(string asmline)
{
    int i=0;
    string param;
    i=asmline.find(" ");
    param=asmline.substr(i+1);          //me quedo con lo que tengo despues del espacio->parametro
    return stoi(param);
}
int help()
{

}
int main(int n,char *arg[])
{
    string linea;                   //cadena de caracteres llamado linea
    int opcode, param;              //defino LOAD,ADD,CODE,PRINT CON SUS PARAMETROS.
    unsigned int word;

    if(n != 1)                      //cheque que N es distinto de 1
    {
        help();
        return 0;
    }

    ifstream archivo ("file.txt", ios::in);         //cargo el archivo

    if (archivo.is_open())
    {
        while ( getline (archivo,linea) )           //obtengo la linea del txt
        {
            opcode= getopcode(linea);               //obtengo opcode ->llamo a la funcion y mando la linea
            param=getparam(linea);                  //obtengo el parámetro
            word=(opcode<<24)|(param&0xFFFFFF);         //desplazo 24 lugares a las derecha

            ofstream out("file.out", ios::app);             //creo el archivo en binario

            if (out.is_open())
            {
                out<<hex<<word<<"\n";
            }
            out.close();
        }
    }
}
